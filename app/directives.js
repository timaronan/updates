app.directive('formElement', function() {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            label : "@",
            model : "="
        },
        link: function(scope, element, attrs) {
            scope.disabled = attrs.hasOwnProperty('disabled');
            scope.required = attrs.hasOwnProperty('required');
            scope.pattern = attrs.pattern || '.*';
        },
        template: '<div class="form-group"><label class="col-sm-3 control-label no-padding-right" >  {{label}}</label><div class="col-sm-9"><span class="block input-icon input-icon-right" ng-transclude></span></div></div>'
      };
        
});

app.directive('onlyNumbers', function() {
    return function(scope, element, attrs) {
        var keyCode = [8,9,13,37,39,46,48,49,50,51,52,53,54,55,56,57,96,97,98,99,100,101,102,103,104,105,110,190];
        element.bind("keydown", function(event) {
            if($.inArray(event.which,keyCode) == -1) {
                scope.$apply(function(){
                    scope.$eval(attrs.onlyNum);
                    event.preventDefault();
                });
                event.preventDefault();
            }

        });
    };
});

app.directive('appFilereader', function($q) {
    var slice = Array.prototype.slice;

    return {
        restrict: 'A',
        require: '?ngModel',
        link: function(scope, element, attrs, ngModel) {
                if (!ngModel) return;

                ngModel.$render = function() {};

                element.bind('change', function(e) {
                    var element = e.target;

                    $q.all(slice.call(element.files, 0).map(readFile))
                        .then(function(values) {
                            if (element.multiple) ngModel.$setViewValue(values);
                            else ngModel.$setViewValue(values.length ? values[0] : null);
                        });

                    function readFile(file) {
                        var deferred = $q.defer();

                        var reader = new FileReader();
                        reader.onload = function(e) {
                            deferred.resolve(e.target.result);
                        };
                        reader.onerror = function(e) {
                            deferred.reject(e);
                        };
                        reader.readAsDataURL(file);

                        return deferred.promise;
                    }

                }); //change

            } //link
    }; //return
});

app.directive('focus', function() {
    return function(scope, element) {
        element[0].focus();
    }      
});

app.directive('animateOnChange', function($animate) {
  return function(scope, elem, attr) {
      scope.$watch(attr.animateOnChange, function(nv,ov) {
        if (nv!=ov) {
              var c = 'change-up';
              $animate.addClass(elem,c, function() {
              $animate.removeClass(elem,c);
          });
        }
      });  
  }  
});

app.directive("toggle", function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            $(elem).on('click', function(){
                var $toggler = $(elem).closest('section').find('.descript');
                $(elem).toggleClass('fa-expand').toggleClass('fa-times').toggleClass('open');
 
                var txt = $toggler.is(':visible') ? 'view info' : 'hide info';
                $(elem).find('span').text(txt);

                $toggler.slideToggle();
                if( $(elem).closest('section').hasClass('complete') ){
                    $(elem).closest('section').find('.site-info').slideToggle();
                }
                    console.log("fire toggle");                                    
            });
        }
    }
});

app.directive("warntoggle", function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            $(elem).on('click', function(){
                var $tb = $(elem).closest('section').find('.info-toggle');

                if($tb.hasClass('open')){
                    $tb.removeClass('open');
                }
                if($tb.hasClass('fa-times')){
                    $tb.removeClass('fa-times');
                } 
                if(!$tb.hasClass('fa-expand')){
                    $tb.addClass('fa-expand');
                } 
                $(elem).closest('section').find('.descript').slideUp();               
                $tb.find('span').text('view info');   

                if( $(elem).hasClass('btn-warning') ){
                    $(elem).closest('section').find('.site-info').slideUp();         
                    console.log("fire if");                                    
                }else{              
                    $(elem).closest('section').find('.site-info').slideDown();         
                    console.log("fire else");                                    
                }
            });
        }
    }
});

app.directive("undotoggle", function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            $(elem).on('mouseenter', function(){
                $(elem).text('mark as incomplete');
            }).on('mouseleave', function(){
                $(elem).text('complete');
            });
        }
    }
});

app.directive("hideone", function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            setTimeout(function(){
                if( $(elem).closest('section').hasClass('complete') ){
                    $(elem).css( 'display' , 'none' );
                }
            }, 100);
        }
    }
});

app.directive("smoothscroll", function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {

            var target = $('#'+attrs.scroll);

            $(elem).click(function(){

                $('.modal').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;

            });
        }
    }
});


