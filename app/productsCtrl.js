app.controller('productsCtrl', function ($scope, $http, $modal, $filter, Data) {
    $scope.update = {};
    $scope.comments = {};
    Data.get('updates').then(function(data){
        $scope.updates = data.data;
    });
    $scope.changeProductStatus = function(update,c){
        if (confirm("Are you sure you want to change the status?") == true) {
            if(c){
                update.complete = (update.complete==0 ? 1 : 0);
                Data.put("updates/"+update.id,{complete:update.complete});
                if(update.complete === 1){
                    update.status = "Incomplete";
                }else{
                    update.status = "Complete";
                }
                $scope.submitStatus(update);
            }else{
                update.working = (update.working==0 ? 1 : 0);
                Data.put("updates/"+update.id,{working:update.working});
                if(update.working === 1){
                    update.status = "Working On";
                }else{
                    update.status = "Static";
                }
                $scope.submitStatus(update);
            }
        } else {
    
        }
    };
    $scope.deleteProduct = function(update){
        if(confirm("Are you sure to remove the update")){
            Data.delete("updates/"+update.id).then(function(result){
                $scope.updates = _.without($scope.updates, _.findWhere($scope.updates, {id:update.id}));
            });
        }
    };
    $scope.open = function (p,size) {
        var modalInstance = $modal.open({
          templateUrl: 'partials/productEdit.html',
          controller: 'updateEditCtrl',
          size: size,
          resolve: {
            item: function () {
              return p;
            }
          }
        });
        modalInstance.result.then(function(selectedObject) {
            if(selectedObject.save == "insert"){
                var newDate = new Date();
                selectedObject.date_requested = newDate;                
                $scope.updates.push(selectedObject);
                $scope.updates = $filter('orderBy')($scope.updates, 'id', 'reverse');
                $scope.submitForm(selectedObject);
            }else if(selectedObject.save == "update"){
                p.assigned_to = selectedObject.assigned_to;
                p.client_name = selectedObject.client_name;
                p.client_website = selectedObject.client_website;
                p.description = selectedObject.description;
                p.file_upload = selectedObject.file_upload;
                $scope.submitForm(selectedObject);
                $scope.edited(selectedObject);
            }
        });
    };

    $scope.storeComments = function (p) {
        Data.get('comment/'+p).then(function(data){
            $scope.comments[p] = data.data;
            var commentlist = $scope.comments[p];
            var comment = {};
            comment.update_id = p;
            comment.commenter = '';
            comment.comment = '';
            var pass = {'comment' : comment, 'commentlist' :commentlist, 'comments' : $scope.comments};

            var modalInstance = $modal.open({
              templateUrl: 'partials/commentView.html',
              controller: 'commentViewCtrl',
              resolve: {
                item: function () {
                  return pass;
                }
              }
            });

            modalInstance.result.then(function(selectedObject) {

            });

        });
    };

    var param = function(data) {
        var returnString = '';
        for (d in data){
            if (data.hasOwnProperty(d))
               returnString += d + '=' + data[d] + '&';
        }
        // Remove last ampersand and return
        return returnString.slice( 0, returnString.length - 1 );
    }; 
    $scope.edited = function(di){
        di.edited = new Date();
        delete di.save;
        Data.put('updates/'+di.id, di).then(function (result) {
            if(result.status != 'error'){
            }else{
                alert(result.message);
            }
        });       
    }  
    $scope.submitForm = function(pazz) {
        var emailer = {}
        emailer.email = pazz.assigned_to.toLowerCase()+'@highrankwebsites.com';
        emailer.name = pazz.assigned_to;
        emailer.emailfrom = pazz.requested_by;
        emailer.message = pazz.description;
        emailer.subject = pazz.client_name;
        $http({
            method : 'POST',
            url : 'api/v1/mail.php',
            data : param(emailer), // pass in data as strings
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
        })
        .success(function(data) {
            if (!data.success) {
                // if not successful, bind errors to error variables
                $scope.errorName = data.errors.name;
                $scope.errorEmail = data.errors.email;
                $scope.errorTextarea = data.errors.message;
                $scope.submissionMessage = data.messageError;
                $scope.submission = true; //shows the error message
                // alert($scope.submissionMessage);
            } else {
                // if successful, bind success message to message
                $scope.submissionMessage = data.messageSuccess;
                $scope.formData = {}; // form fields are emptied with this line
                $scope.submission = true; //shows the success message
                // alert($scope.submissionMessage);
            }
        });
    };
    $scope.submitStatus = function(pazz) {
        var emailer = {}
        if( pazz.status === 'Complete'){
            var passer = pazz.requested_by.toLowerCase()+'@ilawyermarketing.com'
            emailer.email_to = "brenda@highrankwebsites.com, tim@highrankwebsites.com, isaac@highrankwebsites.com, hrwupdates@gmail.com, "+passer;
        }else{
            emailer.email_to = "hrwupdates@gmail.com";
        }
        emailer.assigned = pazz.assigned_to;
        emailer.status = pazz.status;
        emailer.request = pazz.requested_by;
        emailer.client = pazz.client_name;
        emailer.date = pazz.date_requested;
        emailer.message = pazz.description;
        console.log(pazz, emailer);
        $http({
            method : 'POST',
            url : 'api/v1/mail-updates.php',
            data : param(emailer), // pass in data as strings
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
        })
        .success(function(data) {
            if (!data.success) {
                // if not successful, bind errors to error variables
                $scope.errorName = data.errors.name;
                $scope.errorEmail = data.errors.email;
                $scope.errorTextarea = data.errors.message;
                $scope.submissionMessage = data.messageError;
                $scope.submission = true; //shows the error message
                // alert($scope.submissionMessage);
            } else {
                // if successful, bind success message to message
                $scope.submissionMessage = data.messageSuccess;
                $scope.formData = {}; // form fields are emptied with this line
                $scope.submission = true; //shows the success message
                // alert($scope.submissionMessage);
            }
        });
    };

});

app.controller('updateEditCtrl', function ($scope, Upload, $modalInstance, item, Data) {
  $scope.update = angular.copy(item);
        
        $scope.cancel = function () {
            $modalInstance.dismiss('Close');
        };
        $scope.popHead = (item.id > 0) ? 'Edit Update' : 'Add Update';
        $scope.buttonText = (item.id > 0) ? 'Save Update' : 'Add New Update';

        var original = item;
        $scope.isClean = function() {
            return angular.equals(original, $scope.update);
        }
        $scope.saveRequest = function (update) {
            if(update.picFile){
                update.file_path = update.picFile.name;
                uploadPic(update.picFile, update);                
            }
            delete update.picFile;
            if(update.id > 0){
                // if (update.file_path.length < 1) {
                    Data.put('updates/'+update.id, update).then(function (result) {
                        if(result.status != 'error'){
                            var x = angular.copy(update);
                            x.save = 'update';
                            $modalInstance.close(x);
                        }else{
                            alert(result.message);
                        }
                    });
                // };
            }else{
                // if (update.file_path.length < 1) {
                    update.complete = 1;
                    Data.post('updates', update).then(function (result) {
                        if(result.status != 'error'){
                            var x = angular.copy(update);
                            x.save = 'insert';
                            x.working = 0;
                            x.id = result.data;
                            $modalInstance.close(x);
                        }else{
                            alert(result.message);
                        }
                    });
                // };
            }
        };          

        function uploadPic(file, update) {
            file.upload = Upload.upload({ 
              url: 'api/v1/uploads.php',
              file: file
            });

            file.upload.then(function (response) {
                file.result = response.data;

                update.file_path = file.result;
                console.log(file.result, update);
                delete update.picFile;            

                if(update.id > 0){
                    Data.put('updates/'+update.id, update).then(function (result) {
                        if(result.status != 'error'){
                            var x = angular.copy(update);
                            x.save = 'update';
                            $modalInstance.close(x);
                            console.log(response.data, update);
                        }else{
                            alert(result.message);
                        }
                    });
                }else{
                    update.complete = 1;
                    Data.post('updates', update).then(function (result) {
                        if(result.status != 'error'){
                            var x = angular.copy(update);
                            x.save = 'insert';
                            x.working = 0;
                            x.id = result.data;
                            $modalInstance.close(x);
                        }else{
                            alert(result.message);
                        }
                    });
                }


            }, function (response) {
              if (response.status > 0)
                $scope.errorMsg = response.status + ': ' + response.data;
            });
        }
});

app.controller('commentViewCtrl', function ($scope, $modalInstance, item, Data) {

        $scope.commentlist = angular.copy(item.commentlist);
        $scope.thecomment = angular.copy(item.comment);
        $scope.comments = angular.copy(item.comments);
        
        $scope.cancel = function () {
            $modalInstance.dismiss('Close');
        };

        $scope.popHead = (item.id > 0) ? 'Edit Comment' : 'Add Comment';
        $scope.buttonText = (item.id > 0) ? 'Save Comment' : 'Add New Comment';

        var original = item;
        $scope.isClean = function() {
            return angular.equals(original, $scope.comment);
        } 
        $scope.saveRequest = function (comment) {
            var newcomment = angular.copy(comment);
            $scope.comments[comment.update_id].push(newcomment);

            var newDate = new Date();
            newcomment.added = newDate;    
            $scope.commentlist.push(newcomment);

            Data.post('comments', comment).then(function (result) {
                $scope.thecomment.commenter = '';
                $scope.thecomment.comment = '';

                if(result.status != 'error'){
                    var x = angular.copy(comment);
                }else{
                    alert(result.message);
                }
            });
        };
});