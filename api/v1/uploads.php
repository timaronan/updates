<?php

    if ( 0 < $_FILES['file']['error'] ) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
    else {
	    $filename = $_FILES['file']['name'];
	    $pp = strpos($filename, '.');
	    $nim = substr($filename, $pp, strlen($filename) - $pp);
	    $new_image_name = 'image_' . date('Y-m-d-H-i-s') . '_' . uniqid() . $nim;
	    if (!move_uploaded_file(
	        $_FILES['file']['tmp_name'],
	        sprintf('../../uploads/' . $new_image_name,
	            sha1_file($_FILES['file']['tmp_name']),
	            $ext
	        )
	    )) {
	        throw new RuntimeException('Failed to move uploaded file.');
	    }
	    echo $new_image_name;
    }

?>