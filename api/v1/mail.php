<?php
$errors = array(); // array to hold validation errors
$data = array(); // array to pass back data
// validate the variables ======================================================
if (empty($_POST['name']))
$errors['name'] = 'Name is required.';
if (empty($_POST['email']))
$errors['email'] = 'Email is required.';
// return a response ===========================================================
// response if there are errors
if ( ! empty($errors)) {
  // if there are items in our errors array, return those errors
  $data['success'] = false;
  $data['errors'] = $errors;
  $data['messageError'] = 'Please check the fields in red';
} else {
  // if there are no errors, return a message
  $data['success'] = true;
  // CHANGE THE TWO LINES BELOW
  $email = $_POST['email']; // required
  $subject = $_POST['subject']; // required
  $email_subject = "Highrank Post-Live ".$subject." Updates";
  $name = $_POST['name']; // required
  $email_to = $name."@highrankwebsites.com";
  $data['messageSuccess'] = 'email deployed to '.$email_to;
  $email_from = $_POST['emailfrom']; // required
  $message = $_POST['message']; // required
  $email_message = "";
  $email_message .= "Update for ".$name." sent -- ";
  $email_message .= " From Highrank Post-Live Updates -- ";
  $email_message .= " Message: ".$message." ";
  $headers = 'From: updates@highrankwebsites.com'.
  @mail($email_to, $email_subject, $email_message, $headers);
}
// return all our data to an AJAX call
echo json_encode($data);