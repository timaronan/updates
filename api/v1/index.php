<?php
require '.././libs/Slim/Slim.php';
require_once 'dbHelper.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$app = \Slim\Slim::getInstance();
$db = new dbHelper();

/**
 * Database Helper Function templates
 */
/*
select(table name, where clause as associative array)
insert(table name, data as associative array, mandatory column names as array)
update(table name, column names as associative array, where clause as associative array, required columns as array)
delete(table name, where clause as array)
*/


// Updates
$app->get('/updates', 'getUpdates');
$app->post('/updates', 'postUpdates');
$app->put('/updates/:id', 'putUpdate');
$app->delete('/updates/:id', 'deleteUpdate');

// Comments
$app->get('/comment/:id', 'getComment');
$app->get('/comments', 'getComments');
$app->post('/comments', 'postComments');
$app->put('/comments/:id', 'putComment');
$app->delete('/comments/:id', 'deleteComment');

function getUpdates() {
    global $db;
    $td = date("Y-m-d H:i:s");
    $condition = array("complete" => 1, "date_requested" => date("Y-m-d H:i:s", strtotime('-7 days')));
    $rows = $db->select("updates","id,client_name,client_website,date_requested,description,requested_by,assigned_to,high_tail,high_tail_folder,complete,working,edited,file_path", $condition);
    echoResponse(200, $rows);
}
function postUpdates(){ 
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $mandatory = array('client_name', 'client_website', 'requested_by');
    global $db;
    $rows = $db->insert("updates", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Update created successfully.";
    echoResponse(200, $rows);
}
function putUpdate($id){ 
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $condition = array('id'=>$id);
    $mandatory = array();
    global $db;
    $rows = $db->update("updates", $data, $condition, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Update updated successfully.";
    echoResponse(200, $rows);
}
function deleteUpdate($id) { 
    global $db;
    $rows = $db->delete("updates", array('id'=>$id));
    if($rows["status"]=="success")
        $rows["message"] = "Update removed successfully.";
    echoResponse(200, $rows);
}

function getComments() {
    global $db;
    $rows = $db->select("comments","id,update_id,commenter,comment,added", array());
    echoResponse(200, $rows);
}
function getComment($id) {
    global $db;
    $condition = array('update_id'=>$id);
    $rows = $db->select("comments","id,update_id,commenter,comment,added", $condition);
    echoResponse(200, $rows);
}
function postComments(){ 
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $mandatory = array('commenter', 'comment');
    global $db;
    $rows = $db->insert("comments", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Comment created successfully.";
    echoResponse(200, $rows);
}
function putComment($id){ 
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $condition = array('id'=>$id);
    $mandatory = array();
    global $db;
    $rows = $db->update("comments", $data, $condition, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Comment updated successfully.";
    echoResponse(200, $rows);
}
function deleteComment($id) { 
    global $db;
    $rows = $db->delete("comments", array('id'=>$id));
    if($rows["status"]=="success")
        $rows["message"] = "Comment removed successfully.";
    echoResponse(200, $rows);
}


function echoResponse($status_code, $response) {
    global $app;
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response,JSON_NUMERIC_CHECK);
}

$app->run();
?>