<?php
$errors = array(); // array to hold validation errors
$data = array(); // array to pass back data
// validate the variables ======================================================
// return a response ===========================================================
// response if there are errors
if ( ! empty($errors)) {
  // if there are items in our errors array, return those errors
  $data['success'] = false;
  $data['errors'] = $errors;
  $data['messageError'] = 'Please check the fields in red';
} else {
  // if there are no errors, return a message
  $data['success'] = true;
  // CHANGE THE TWO LINES BELOW
  $assigned = $_POST['assigned']; // required
  $status = $_POST['status']; // required
  $request = $_POST['request']; // required
  $client = $_POST['client']; // required
  $date = $_POST['date']; // required
  $message = $_POST['message']; // required
  $email_subject = $status." iLawyer Post-Live ".$assigned." says for ".$client." update added ".$date;
  $email_to = $_POST['email_to'];
  $data['messageSuccess'] = 'email deployed to '.$email_to;
  $email_from = $_POST['emailfrom']; // required
  $email_message = "";
  $email_message .= $name." changed the status of update for ".$client." assigned on ".$date." by ".$request." to ".$status;
  $email_message .= " From Highrank Post-Live Updates -- ";
  $email_message .= " Message: ".$message." ";
  $headers = 'From: updates@ilawyermarketing.com';
  @mail($email_to, $email_subject, $email_message, $headers);
}
// return all our data to an AJAX call
echo json_encode($data);